package com.wumpafruit.notes;

import java.util.ArrayList;

import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {
	public static final String PREFS_NAME = "my_notes";
	private ArrayList<String> notes;
	public static int noteListSize = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		
		notes = new ArrayList<String>();
		
		//Populate list of notes
		SharedPreferences noteList = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		
		String note = noteList.getString("note0", null);
		int i = 1;
		while (note != null) {
			notes.add(note);
			note = noteList.getString("note" + (i++), null);
		}
		noteListSize = notes.size();
		
		View layout = (LinearLayout) findViewById(R.id.container);
		
		for (int j = 0; j < notes.size(); j++) {
			TextView textView = new TextView(this);
			LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
			textView.setLayoutParams(layoutParams);
			textView.setText(notes.get(j));
			((LinearLayout) layout).addView(textView);
		}
		
		Log.e("my_notes", "Showing " + notes.size() + " notes");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
			case R.id.action_create_new_note:
				createNewNote();
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private void createNewNote() {
		Intent intent = new Intent(this, CreateNoteActivity.class);
		startActivity(intent);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}
}
