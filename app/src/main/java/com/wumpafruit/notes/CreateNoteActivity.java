package com.wumpafruit.notes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class CreateNoteActivity extends Activity implements OnEditorActionListener{
	private EditText editText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_note);
		editText = (EditText) findViewById(R.id.new_note_content);
		editText.setOnEditorActionListener(this);
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		boolean handled = false;
		
		if (actionId == EditorInfo.IME_ACTION_DONE) {
			saveNote(null);
			handled = true;
		}
		
		return handled;
	}

	public void saveNote(View view) {		
		SharedPreferences noteList = getSharedPreferences(MainActivity.PREFS_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = noteList.edit();
		editor.putString("note" + MainActivity.noteListSize, editText.getText().toString());
		editor.commit();
		
		//Show a toast at the bottom to acknowledge their note was saved
		Context context = getApplicationContext();
		CharSequence toastText = "Note saved";
		int duration = Toast.LENGTH_SHORT;
		
		Toast toast = Toast.makeText(context, toastText, duration);
		toast.show();
		
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);	
	}
}
